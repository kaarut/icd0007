<?php
require_once("functions.php");

    $connection = pdo_mysql();

    $errors = [];
    $success = 'save';
    $table = 'authors';

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $errors = saveValid($table);
        if (empty($errors)) {
            addBlocks($table, $connection);
            header("Location: author_view.php?success=Lisatud");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bookshelf</title>
    <link type="text/css" rel="stylesheet" src="style.css" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link type="text/js" href="functions.js">
</head>
<body>
<?php require 'parts/menu.html' ?>
<main>
    <div class="container">
        <?php foreach ($errors as $error): ?>
            <div id="error-block" class="error"><?= $error ?></div>
            <br>
        <?php endforeach; ?>
    </div>
    <div class="book_form">
        <form action="author_add.php" method="post" name="author_add">
            <div class="cell">
                <label for="firstName">Eesnimi:</label>
                <input id="firstName" name="firstName" type="text"
                       value="<?php echo empty($errors) ? "" : $_POST["firstName"]?>">
            </div>
            <div class="cell">
                <label for="lastName">Perekonnanimi</label>
                <input id="lastName" name="lastName" type="text"
                       value="<?php echo empty($errors) ? "" : $_POST["lastName"]?>">
            </div>
            <div class="cell">
                <label for="grade">Hinnang: </label>
                <?php $grade = $_POST["grade"]?>
                <?php
                $arr = array(1,2,3,4,5); foreach($arr as $int)
                    if ($int != $grade)
                        echo "<input name='grade' value='$int' type='radio'>$int";
                    else
                        echo "<input name='grade' checked value='$int' type='radio'>$int";
                ?>
            </div>
            <div class="cell">
                <input name="submitButton" type="submit" value="Kinnitan">
            </div>
        </form>
    </div>
</main>
<?php require 'parts/footer.html' ?>
</body>
</html>