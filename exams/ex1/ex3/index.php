<?php

require_once '../vendor/tpl.php';
require_once 'Person.php';


$firstName = isset($_POST['firstName']) ? $_POST['firstName'] : '';
$lastName = isset($_POST['lastName']) ? $_POST['lastName'] : '';
$phone = isset($_POST['phone']) ? $_POST['phone'] : '';

$person = new Person($firstName, $lastName, $phone);
$personData = urlencode(serialize($person));

if ($_POST['previewButton']){

    $data = [
        'fileName' => 'preview.html',
        'person' => $person,
        'personData' => $personData
    ];

    print renderTemplate('main.html', $data);
}
elseif ($_POST['backButton']){

        $data = [
            'fileName' => 'form.html',
            'person' => unserialize(urldecode($_POST['personData']))
        ];
        print renderTemplate('main.html', $data);
}
else {

    $data['fileName'] = 'form.html';
    print renderTemplate('main.html', $data);

}

