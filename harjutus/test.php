<?php

require_once("Person.php");

$person = new Person("John", "Doe", ["+37258867349"]);
print json_encode($person, JSON_PRETTY_PRINT | JSON_PRESERVE_ZERO_FRACTION);