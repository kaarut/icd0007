<?php

require_once "Book.php";

class DaoBooks
{

    function __construct()
    {

        // connection settings
        $this->db_host = 'mkalmo.xyz';
        $this->db_user = 'kaarut';
        $this->db_pass = '02F3';
        $this->db_name = 'kaarut';

    }

    function __destruct()
    {
        // close connections when the object is destroyed
        $this->dbh = null;
    }

    public function db_connect()
    {
        try {

            $dbh = new PDO("mysql:host=$this->db_host;dbname=$this->db_name",
                $this->db_user, $this->db_pass,
                array(PDO::ATTR_PERSISTENT => true));

            return $dbh;

        } catch (PDOException $e) {
            // eventually write this to a file
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function getBooks()
    {
        $connection = $this->db_connect();
        $books = $connection->prepare(
            "SELECT books.id id, title, books.grade, 
                group_concat(CONCAT(authors.firstName, ' ', authors.lastName) SEPARATOR ', ') as 'authors'
         FROM books
         LEFT JOIN bookAuthors ON books.id = bookAuthors.book_id 
         LEFT JOIN authors ON bookAuthors.author_id = authors.id
         GROUP BY books.id");

        $books->execute();
        while($book = $books->fetch(PDO::FETCH_ASSOC)){
            $authorsData = explode(", ", $book['authors']);
            $authors=[];
            foreach ($authorsData as $author){
                $author = explode(" ", $author);
                array_push($authors, new Author($author[0], $author[1]));
            }
//            var_dump($authors);
//            echo "<br>";
            $book_list[] = new Book($book['title'], $book['grade'], $book['isRead'], $authors, $book['id']);
        }
        return $book_list;
    }


    public function getBookById($id)
    {
        $connection = $this->db_connect();
        $books = $connection->prepare(
            "SELECT books.id id, title, books.grade, 
                group_concat(CONCAT(authors.firstName, ' ', authors.lastName, ' ', authors.grade, ' ', authors.id) SEPARATOR ', ') as 'authors'
         FROM books
         LEFT JOIN bookAuthors ON books.id = bookAuthors.book_id 
         LEFT JOIN authors ON bookAuthors.author_id = authors.id
         WHERE books.id = ?
         GROUP BY books.id");

        $books->execute([$id]);
        $book = $books->fetch(PDO::FETCH_ASSOC);
        $authorsData = explode(", ", $book['authors']);
        $authors=[];
        foreach ($authorsData as $author){
            $author = explode(" ", $author);
            array_push($authors, new Author($author[0], $author[1], $author[2], $author[3]));}
        $book = new Book($book['title'], $book['grade'], $book['isRead'], $authors, $book['id']);
        return $book;
    }

    public function saveBook($book)
    {
        $connection = $this->db_connect();
        $title = $book->title;
        $grade = $book->grade;
        $isRead = $book->isRead;

        $authors = $book->authors;
        $author1 = $authors[0];
        $author2 = $authors[1];

        $stmt = $connection->prepare('INSERT INTO books set title = :title, 
        isRead = :isRead, grade = :grade');
        $stmt->bindvalue(':title', $title, PDO::PARAM_STR);
        $stmt->bindvalue(':isRead', $isRead, PDO::PARAM_STR);
        $stmt->bindvalue(':grade', $grade, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $bookId = $connection->lastInsertId();
            if ($author1) {
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id)
            VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$bookId, $author1->id]);
            }
            if ($author2) {
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id)
            VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$bookId, $author2->id]);
            }
        }
    }

    public function bookRemove($id)
    {
        $connection = $this->db_connect();
        $pdo = $connection->prepare("DELETE FROM books where id = ?");
        $pdo->execute([$id]);
    }


    public function editBook($book, $id)
    {
        $connection = $this->db_connect();
        $title = $book->title;
        $grade = $book->grade;
        $isRead = $book->isRead;
        $authors = $book->authors;
        $author1 = $authors[0];
        $author2 = $authors[1];

        $stmt = $connection->prepare('UPDATE books set title = :title, 
    isRead = :isRead, grade = :grade where id = :id');
        $stmt->bindvalue(':title', $title, PDO::PARAM_STR);
        $stmt->bindvalue(':isRead', $isRead, PDO::PARAM_STR);
        $stmt->bindvalue(':grade', $grade, PDO::PARAM_INT);
        $stmt->bindvalue(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $sql_del = "DELETE FROM bookAuthors WHERE book_id = ?";
            $sth = $connection->prepare($sql_del);
            $sth->execute([$id]);
            if ($author1) {
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id) VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$id, $author1->id]);
            }
            if ($author2) {
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id) VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$id, $author2->id]);
            }
        }
    }
}
