<?php
require_once("functions.php");

    $connection = pdo_mysql();
    $authorios = [];

    $authors = getAuthorNames($connection);
    foreach ($authors as $row){
        array_push($authorios, $row);
    }

    $errors = [];
    $success = 'save';
    $table = 'books';

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $errors = saveValid($table);
        if (empty($errors)) {
            addBlocks($table, $connection);
            header("Location: index.php?success=Lisatud");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bookshelf</title>
    <link type="text/css" rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
<?php require 'parts/menu.html' ?>
<main>
    <div class="container">
        <?php foreach ($errors as $error): ?>
            <div id="error-block" class="error"><?= $error ?></div>
            <br>
        <?php endforeach; ?>
    </div>
    <div class="book_form">
        <form action="book_add.php" method="post" name="book_add">
            <div class="cell">
                <label for="title">Pealkiri:</label>
                <input id="title" name="title" type="text"
                value="<?php echo empty($errors) ? "" : $_POST["title"]?>">
            </div>
        <div class="cell">
            <label for="author1">Autor I: </label>
                <select id="author1" name="author1">
                    <option value></option>
                        <?php foreach ($authorios as $name): ?>
                            <option value="<?= $name['id']?>"><?= $name['firstName'] . ' '. $name['lastName']?> </option>
                        <?php endforeach; ?>
                </select>
            </div>
        <div class="cell">
            <label for="author2">Autor II: </label>
                <select id="author2" name="author2">
                    <option value></option>
                        <?php foreach ($authorios as $name_1): ?>
                            <option value="<?= $name_1['id']?>"><?= $name_1['firstName'] . ' '. $name_1['lastName']?> </option>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="cell">
                <label for="grade">Hinnang: </label>
                <?php $grade = $_POST["grade"];
                $arr = array(1,2,3,4,5); foreach($arr as $int)
                    if ($int != $grade)
                        echo "<input name='grade' value='$int' type='radio'>$int";
                    else
                        echo "<input name='grade' checked value='$int' type='radio'>$int";
                ?>
            </div>
            <div class="cell">
                <label for="read">Loetud: </label>
                <?php $isRead = $_POST["isRead"]?>
                <input id="read" value="1" name="isRead" type="checkbox"
                    <?php if ($isRead== '1') {echo "checked";}?>>
            </div>
            <div class="cell">
                <input name="submitButton" type="submit" value="Kinnitan">
            </div>
        </form>
    </div>
</main>
<?php require 'parts/footer.html' ?>
</body>
</html>