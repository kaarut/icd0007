<?php

class Book {

    public $title;
    public $grade;
    public $isRead;
    public $authors;
    public $id;

    public function __construct($title, $grade, $isRead, $authors, $id=0) {
        $this->title = $title;
        $this->grade = $grade;
        $this->isRead = $isRead;
        $this->authors = $authors;
        $this->id = $id;
    }

    public function addAuthor($author) {
        $this->authors[] = $author;
    }

    public function getAuthorsString(){
        $array = [];
        foreach ($this->authors as $author){
            array_push($array,$author->toString());
        }
        return implode(", ", $array);
    }

}
