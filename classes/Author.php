<?php

class Author {

    public $id;
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($firstName = "", $lastName = "", $grade=0, $id=0) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;
        $this->id = $id;
    }

    public function toString(){
        return $this->firstName . " " . $this->lastName;
    }
}
