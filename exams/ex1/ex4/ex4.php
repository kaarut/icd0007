
<?php

require_once '../ex4/functions.php';
require_once 'MenuItem.php';

$conn = getConnectionWithData('data.sql');

$stmt = $conn->prepare('SELECT id, parent_id, name 
                        FROM menu_item ORDER BY id');

$stmt->execute();

$menu = []; // siia peab jääma lõpptulemus.

foreach ($stmt as $item) {

    $id = $item['id'];
    $name = $item['name'];
    $parentId = $item['parent_id'];

    if($parentId === null) {
        $item1 = new MenuItem($id, $name);
//        $menu [] = $item1;
    }

    if($parentId != null) {
        $item1->addSubItem(new MenuItem($id, $name));
        $menu [] = $item1;
    }
}
// kood tuleb siia

printMenu($menu); // lihtsalt tulemuse vaatamiseks