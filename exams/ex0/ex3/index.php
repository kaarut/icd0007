<?php

require_once '../vendor/tpl.php';

$message = urlencode(isset($_POST["message"]))
    ? $_POST["message"]
    : "";

if (isset($_POST['message'])) {
    header('HTTP/1.1 302 See result');
    header("Location: ?message=$message");

}

elseif (isset($_GET['message'])) {
    $word = $_GET['message'];

    $data = [
        'message' => strrev(urldecode($word))
    ];

    print renderTemplate('page.html', $data);
}

else {
    print renderTemplate('form.html');
}

//
//require_once '../vendor/tpl.php';
//
//$message = urlencode(isset($_POST["message"]))
//    ? $_POST["message"]
//    : "";
//
//$button = $_POST['button'] ?? null;
//
//if (isset($button)) {
//
//    if(!empty($message)) {
//        $message = urlencode(strrev($message));
//
//        $data = [
//            'message' => urldecode(strrev($message))
//        ];
//
//        print renderTemplate('page.html', $data);
//    }
//    else {
//        header('Location: localhost:8080');
//        exit();
//    }
//
//} else {
//    print renderTemplate('form.html');
//}




