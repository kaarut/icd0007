<?php
    require_once("functions.php");

    $connection = pdo_mysql();
    $authors = get_authors($connection);
//    foreach($authors as $row) {
//        print $row['id'] . ' ';
//        print $row['firstName'] . ' ';
//        print $row['lastName'] . ' ';
//        print $row['grade'] . PHP_EOL;
//    }
    $success = isSuccess();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bookshelf</title>
    <link type="text/css" rel="stylesheet" src="style.css" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link type="text/js" href="functions.js">
</head>
<body>
<?php require 'parts/menu.html' ?>
<main>
    <?=$success?>
    <div id="book_table">
        <table>
            <tr>
                <th>Eesnimi</th>
                <th>Perekonnanimi</th>
                <th>Hinne</th>
            </tr>
            <?php foreach ($authors as $row):?>
                <tr>
                    <td><a href="author_edit.php?id=<?=$row['id']?>"><?= $row["firstName"] ?></a></td>
                    <td><?= $row["lastName"] ?></td>
                    <td><?= str_repeat('★', (int)$row["grade"])?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</main>
<?php require 'parts/footer.html' ?>
</body>
</html>