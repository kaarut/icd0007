<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex3Tests extends WebTestCaseExtended {

    function defaultPageIsEmptyFrom() {
        $this->get(BASE_URL);

        $this->assertField('firstName', '');
    }

    function submittingTheFromShowsPreviewPage() {
        $this->get(BASE_URL);

        $this->setFieldByName('firstName', 'Alice');
        $this->setFieldByName('lastName', 'Smith');
        $this->setFieldByName('phone', '123');

        $this->clickSubmitByName('previewButton');

        $this->assertNoField('firstName');
        $this->assertText('Alice');
        $this->assertText('Smith');
        $this->assertText('123');
    }

    function navigatingBackFromPreviewShowsFilledForm() {
        $this->get(BASE_URL);

        $this->setFieldByName('firstName', 'Alice');
        $this->setFieldByName('lastName', 'Smith');
        $this->setFieldByName('phone', '123');

        $this->clickSubmitByName('previewButton');

        $this->clickSubmitByName('backButton');

        $this->assertField('firstName', 'Alice');
        $this->assertField('lastName', 'Smith');
        $this->assertField('phone', '123');
    }
}

(new Ex3Tests())->run(new PointsReporter(
    [1 => 2,
     2 => 4,
     3 => 10]));


