<?php
function saveValid($table)
{
    $errors = [];
    if ($table == "authors") {
        $firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : "";
        $lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : "";
//        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";
        if (strlen($firstName) > 22 || empty($firstName))
            $errors[] = "Eesnimi peab olema 1 kuni 21 märki!";

        if (strlen($lastName) > 22 || (strlen($lastName) < 2))
            $errors[] = "Perekonnanimi peab olema 2 kuni 22 märki!";

//        if (empty($grade))
//            $errors[] = "Sisesta hinne";
        return $errors;
    }
    elseif ($table == "books") {
        $title = isset($_POST["title"]) ? $_POST["title"] : "";
        $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "";
//        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";

        if (strlen($title) < 3 || (strlen($title) > 23))
            $errors[] = "Pealkiri peab olema 3 kuni 23 märki!";

//        if (empty($grade))
//            $errors[] = "Sisesta hinne";
        return $errors;
    }
}

function isSuccess() {
    $success = isset($_GET["success"]) ? $_GET["success"]: "";
    if (!empty($success))
        $success = "<div class='container'><div id='message-block'>$success!</div></div>";
    return $success;
}

function pdo_mysql()
{
    $USERNAME = 'kaarut';
    $PASSWORD = '02F3';

    $address = 'mysql:host=db.mkalmo.xyz;dbname=kaarut';

    $connection = new PDO($address, $USERNAME, $PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    return $connection;
}

function get_authors($connection)
{
    $authors = $connection->prepare(
        'SELECT id, firstName, lastName, grade FROM authors');

    $authors->execute();
    return $authors;
}

function sortBookAuthors($books){
    $result = [];

    foreach ($books as $book){
        $author = $book['firstName']." ".$book['lastName'];
        $bookId = $book['id'];
        if (key_exists($book['id'], $result)){
            array_push($result[$bookId]["authors"], $author);
        }
        else{
            $result[$bookId] = [
                "id" => $bookId,
                "title" =>$book['title'],
                "grade" => $book['grade'],
                "authors" => [$author]
            ];
        }
    }
    return $result;
}

function getBooks($connection)
{
    $books = $connection->prepare(
        "SELECT books.id id, title, books.grade, 
                group_concat(CONCAT(authors.firstName, ' ', authors.lastName) SEPARATOR ', ') as 'authors'
         FROM books
         LEFT JOIN bookAuthors ON books.id = bookAuthors.book_id 
         LEFT JOIN authors ON bookAuthors.author_id = authors.id
         GROUP BY books.id");

    $books->execute();
    //$books[0]["authors"] = ["Mihkel Raud", "Andrus Kivir2hk", "salajane autor"]
    $books = $books->fetchAll(PDO::FETCH_ASSOC);

    return $books;
//    return sortBookAuthors($books);
}

function addBlocks($table, $connection)
{
    if ($table == "authors") {
        $firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : "";
        $lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : "";
        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";
        $authorSql = $connection -> prepare('INSERT INTO authors set firstName = :firstName,
        lastName = :lastName, grade = :grade');
        $authorSql ->bindvalue(':firstName', $firstName, PDO::PARAM_STR);
        $authorSql ->bindvalue(':lastName', $lastName, PDO::PARAM_STR);
        $authorSql ->bindvalue(':grade', $grade, PDO::PARAM_INT);
        $authorSql -> execute();
    } elseif ($table == "books") {
        $title = isset($_POST["title"]) ? $_POST["title"] : "";
        $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "";
        if (!empty($isRead)) {$isRead = 1;} elseif (empty($isRead)) $isRead = 0;
        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";
        $author_1 = isset($_POST['author1']) ? $_POST['author1'] : "";
        $author_2 = isset($_POST['author2']) ? $_POST['author2'] : "";
        $stmt = $connection->prepare('INSERT INTO books set title = :title, 
        isRead = :isRead, grade = :grade');
        $stmt->bindvalue(':title', $title, PDO::PARAM_STR);
        $stmt->bindvalue(':isRead', $isRead, PDO::PARAM_STR);
        $stmt->bindvalue(':grade', $grade, PDO::PARAM_INT);
//        $sth->bindParam("title", $title);
        if ($stmt->execute()) {
            $bookId = $connection->lastInsertId();
            if ($author_1) {
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id)
            VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$bookId, $author_1]);
            }
            if ($author_2) {
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id)
            VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$bookId, $author_2]);
            }
        }
    }
}
function executeSQL($connection, $sql) {

    $execute = $connection->prepare($sql);
    $execute->execute();

}

function editPost($id, $table, $connection)
{

    if ($table == 'authors') {
        $firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : "";
        $lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : "";
        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";
        #TODO: SEE V2 SIIN
        $stmt = $connection -> prepare('UPDATE authors set firstName = :firstName,
        lastName = :lastName, grade = :grade where id = :id');
        $stmt ->bindvalue(':firstName', $firstName, PDO::PARAM_STR);
        $stmt ->bindvalue(':lastName', $lastName, PDO::PARAM_STR);
        $stmt ->bindvalue(':grade', $grade, PDO::PARAM_INT);
        $stmt ->bindvalue(':id', $id, PDO::PARAM_INT);
        $stmt -> execute();

    } elseif ($table == "books") {
        $title = isset($_POST["title"]) ? $_POST["title"] : "";
        $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "";
        if (!empty($isRead)) {$isRead = 1;} elseif (empty($isRead)) $isRead = 0;
        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";
        $author_1 = isset($_POST['author1']) ? $_POST['author1'] : "";
        $author_2 = isset($_POST['author2']) ? $_POST['author2'] : "";
        $stmt = $connection -> prepare('UPDATE books set title = :title, 
        isRead = :isRead, grade = :grade where id = :id');
        $stmt ->bindvalue(':title', $title, PDO::PARAM_STR);
        $stmt ->bindvalue(':isRead', $isRead, PDO::PARAM_STR);
        $stmt ->bindvalue(':grade', $grade, PDO::PARAM_INT);
        $stmt ->bindvalue(':id', $id, PDO::PARAM_INT);
//        $stmt -> execute();
        if($stmt->execute()){
            $bookId = $id;
            $sql_del = "DELETE FROM bookAuthors WHERE book_id = $bookId";
            $sth = $connection->prepare($sql_del);
//            $sql_del = $connection -> prepare("DELETE FROM bookAuthors WHERE book_id = :bookId");
//            $sql_del -> bindvalue(':bookId', $bookId, PDO::PARAM_STR);
            $sth->execute();
            if($author_1){
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id)VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$bookId, $author_1]);}
            if ($author_2){
                $sql_2 = "INSERT INTO bookAuthors (book_id, author_id)
            VALUES (?, ?)";
                $sth = $connection->prepare($sql_2);
                $sth->execute([$bookId, $author_2]);
            }
        }
    }
}

function deletePost($id, $table, $connection) {

    $sql = "DELETE FROM $table
    where id = $id";
    executeSQL($connection, $sql);
}

function getPostById($connection, $table, $id) {
    $posts = $connection->prepare(
        "SELECT * from $table WHERE id = ?");
    $posts->execute([$id]);
    return $posts;
}

function getBookById($connection, $id) {
    $posts = $connection->prepare(
        "SELECT books.id, title, books.grade, isRead, firstName, lastName FROM books
        LEFT JOIN bookAuthors ON books.id = bookAuthors.book_id
        LEFT JOIN authors ON bookAuthors.author_id = authors.id
        WHERE books.id = ?");
    $posts->execute([$id]);
    $posts = $posts->fetchAll(PDO::FETCH_ASSOC);
    return $posts;
}

function getAuthorNames($connection)
{
    $authors = $connection->prepare(
        'SELECT id, firstName, lastName FROM authors');
    $authors->execute();
    return $authors;
}


function validation($table){
    if ($table == 'books'){
        $title = isset($_POST["title"]) ? $_POST["title"] : "";
        if (strlen($title) < 3 || (strlen($title) > 23))
            $errors[] = "Pealkiri peab olema 3 kuni 23 märki!";
        return $errors;
    }
    else {
        $firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : "";
        $lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : "";
        if (strlen($firstName) > 22 || empty($firstName))
            $errors[] = "Eesnimi peab olema 1 kuni 21 märki!";
        if (strlen($lastName) > 22 || (strlen($lastName) < 2))
            $errors[] = "Perekonnanimi peab olema 2 kuni 22 märki!";
        return $errors;
    }
}


function validateBook($book){
    if (strlen($book->title) < 3 || (strlen($book->title) > 23))
        $errors[] = "Pealkiri peab olema 3 kuni 23 märki!";
    return $errors;
}

function validateAuthor($author){
    if (strlen($author->firstName) > 22 || empty($author->firstName))
        $errors[] = "Eesnimi peab olema 1 kuni 21 märki!";
    if (strlen($author->lastName) > 22 || (strlen($author->lastName) < 2))
        $errors[] = "Perekonnanimi peab olema 2 kuni 22 märki!";
    return $errors;
}

function createBook(){

    $title = isset($_POST["title"]) ? $_POST["title"] : "";
    $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : null;
    if (!empty($isRead)) {$isRead = 1;} elseif (empty($isRead)) $isRead = 0;
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";

    $author1 = new Author();
    $author2 = new Author();

    $author1->id = isset($_POST["author1"]) ? $_POST["author1"] : null;
    $author2->id = isset($_POST["author2"]) ? $_POST["author2"] : null;


    $book = new Book($title, $grade, $isRead, [$author1, $author2]);
    return $book;

}

function createAnAuthor(){
    $firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : "";
    $lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : "";
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : "";
    $author = new Author($firstName, $lastName, $grade);
    return $author;
}

function getMessage(){
    if ($_GET['message'] == 'saved'){
        return 'Salvestatud';
    }
    elseif ($_GET['message'] == 'removed'){
        return 'Kustutatud';
    }
}