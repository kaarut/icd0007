<?php

class Person {
    public $firstName;
    public $lastName;
    public $phoneNumbers;

    public function __construct($firstName, $lastName, $phoneNumbers)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phoneNumbers = $phoneNumbers;
    }

    public function validate() {
        $errors =  [];

        if (strlen($this->firstName) == 0 ) {
            array_push($errors, "eesnimi peab olema täidetud");
        }
        if (empty($this->lastName)) {
            array_push($errors, "perekonnanimi peab olema täidetud");
        }
        if (count($this->phoneNumbers) < 1 ){
            array_push($errors, "vähemalt üks telefoninumber peab olema märgitud");
        }

        return $errors;
    }
}

