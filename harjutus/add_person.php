<?php

require_once("Person.php");

if($_SERVER["REQUEST_METHOD"] === "POST"){
    $data = file_get_contents("php://input");

    $personData = json_decode($data, true);

    $person = new Person($personData["firstName"], $personData["lastName"], $personData["phoneNumbers"]);

    $errors = $person->validate();

    if(!empty($errors)) {
        header("Content-Type: application/json");
        http_response_code(400);
        print json_encode(["errors" => $errors]);
    }
    else {
        header("Content-Type: application/json");
        print json_encode($person);
    }

}
else {
    http_response_code(404);
}