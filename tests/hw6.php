<?php

require_once 'common.php';

const BASE_URL = 'http://localhost:8080/';

class Hw6Tests extends HwTests {

    function testsAreNotReadyYet() {
        $this->fail();
    }
}

(new Hw6Tests())->run(new PointsReporter(0, []));
