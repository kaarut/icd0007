<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex1Tests extends WebTestCaseExtended {

    function indexToC() {
        $this->fetch('/');

        $this->ensureRelativeLink("c.html");
        $this->clickLink("c.html");
        $this->assertCurrentUrlEndsWith("/a/b/c/c.html");
    }

    function cToB() {
        $this->fetch('/a/b/c/c.html');

        $this->ensureRelativeLink("b.html");
        $this->clickLink("b.html");
        $this->assertCurrentUrlEndsWith("/a/b/b.html");
    }

    function bToD() {
        $this->fetch('/a/b/b.html');

        $this->ensureRelativeLink("d.html");
        $this->clickLink("d.html");
        $this->assertCurrentUrlEndsWith("/a/b/c/d/d.html");
    }

    function dToB() {
        $this->fetch('/a/b/c/d/d.html');

        $this->ensureRelativeLink("b.html");
        $this->clickLink("b.html");
        $this->assertCurrentUrlEndsWith("/a/b/b.html");
    }

    function bToIndex() {
        $this->fetch('/a/b/b.html');

        $this->ensureRelativeLink("index.html");
        $this->clickLink("index.html");
        $this->assertCurrentUrlEndsWith("/index.html");
    }

    private function fetch($url) {
        $this->get(BASE_URL . $url);
    }
}

(new Ex1Tests())->run(new PointsReporter(
    [1 => 1,
     2 => 2,
     3 => 3,
     4 => 4,
     5 => 6]));
