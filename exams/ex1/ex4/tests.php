<?php

require_once '../vendor/common.php';

class Ex4Tests extends UnitTestCaseExtended {

    function menuHasTwoRootItems() {
        $menu = [];

        require 'ex4.php';

        $this->assertEqual(sizeof($menu), 2);
        $this->assertEqual($menu[0]->name, 'Item 1');
        $this->assertEqual($menu[1]->name, 'Item 2');
    }

    function firstItemHasCorrectSubStructure() {

        $menu = [];

        require 'ex4.php';

        $this->assertEqual(sizeof($menu[0]->subItems), 2);
        $this->assertEqual($menu[0]->subItems[0]->name, 'Item 1.1');
        $this->assertEqual($menu[0]->subItems[1]->name, 'Item 1.2');
    }

    function secondItemHasCorrectSubStructure() {

        $menu = [];

        require 'ex4.php';

        $this->assertEqual(sizeof($menu[1]->subItems), 1);
        $this->assertEqual($menu[1]->subItems[0]->name, 'Item 2.1');
        $this->assertEqual($menu[1]->subItems[0]->subItems[0]->name, 'Item 2.1.1');
    }

}

(new Ex4Tests())->run(new PointsReporter(
    [1 => 3,
     2 => 7,
     3 => 10]));

