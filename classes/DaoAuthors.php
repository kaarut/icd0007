<?php

require_once "Author.php";

class DaoAuthors
{

    function __construct()
    {

        // connection settings
        $this->db_host = 'mkalmo.xyz';
        $this->db_user = 'kaarut';
        $this->db_pass = '02F3';
        $this->db_name = 'kaarut';

    }

    function __destruct()
    {
        // close connections when the object is destroyed
        $this->dbh = null;
    }

    public function db_connect()
    {
        try {

            $dbh = new PDO("mysql:host=$this->db_host;dbname=$this->db_name",
                $this->db_user, $this->db_pass,
                array(PDO::ATTR_PERSISTENT => true));

            return $dbh;

        } catch (PDOException $e) {
            // eventually write this to a file
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    #TODO: CREATE CRF VALIDATION!
    public function crf($object)
    {
        return $object;
    }

    function smtmBindvalue($stmt, $value)
    {
        $stmt->bindvalue(":$value", $value, PDO::PARAM_STR);
        return $stmt;
    }

    public function get_authors()
    {
        $connection = $this->db_connect();
        $authors = $connection->prepare(
            'SELECT id, firstName, lastName, grade FROM authors ');

        $authors->execute();
        foreach ($authors as $author) {
            $authors_list[] = new Author($author['firstName'], $author['lastName'], $author['grade'], $author['id']);
        }
        return $authors_list;
    }


    public function getAuthorById($id)
    {
        $connection = $this->db_connect();
        $pdo = $connection->prepare(
            'SELECT id, firstName, lastName, grade FROM authors where id = ?');
        $pdo->execute([$id]);
//        while($row =$pdo->fetch())
        $author = $pdo->fetch(PDO::FETCH_ASSOC);
        var_dump($author);
        $new_author = new Author($author['firstName'], $author['lastName'], $author['grade'], $author['id']);
        return $new_author;
    }

    public function saveAuthor($author)
    {
        $connection = $this->db_connect();
        $firstName = $author->firstName;
        $lastName = $author->lastName;
        $grade = $author->grade;

        $authorSql = $connection->prepare('INSERT INTO authors set firstName = :firstName,
        lastName = :lastName, grade = :grade');
        $authorSql->bindvalue(':firstName', $firstName, PDO::PARAM_STR);
        $authorSql->bindvalue(':lastName', $lastName, PDO::PARAM_STR);
        $authorSql->bindvalue(':grade', $grade, PDO::PARAM_INT);
        $authorSql->bindvalue(':grade', $grade, PDO::PARAM_INT);
        $authorSql->execute();
    }

    public function authorRemove($id)
    {
        $connection = $this->db_connect();
        $pdo = $connection->prepare(
            "DELETE FROM authors where id = ?");
        $pdo->execute([$id]);
//        $pdo->debugDumpParams();
    }

    public function authorEdit($author, $id)
    {
        $connection = $this->db_connect();

        $firstName = $author->firstName;
        $lastName = $author->lastName;
        $grade = $author->grade;

        $stmt = $connection->prepare('UPDATE authors set firstName = :firstName,
    lastName = :lastName, grade = :grade where id = :id');
        $stmt->bindvalue(':firstName', $firstName, PDO::PARAM_STR);
        $stmt->bindvalue(':lastName', $lastName, PDO::PARAM_STR);
        $stmt->bindvalue(':grade', $grade, PDO::PARAM_INT);
        $stmt->bindvalue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }
}

