<?php

class Person {
    public $firstName;
    public $lastName;
    public $phone;

    public function __construct($firstName, $lastName, $phone) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;
    }

}

