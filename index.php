<?php

require_once 'vendor/tpl.php';
require_once 'classes/Request.php';
require_once 'classes/Book.php';
require_once 'classes/Author.php';
require_once 'classes/DaoAuthors.php';
require_once 'classes/DaoBooks.php';
require_once 'functions.php';


$request = new Request($_REQUEST);

$daoAuthor = new DaoAuthors();
$daoBook = new DaoBooks();

print $request; // display input parameters (for debugging)


$command = $request->param('command')
    ? $request->param('command')
    : 'show_book_list';

if ($command === 'show_book_list') {
    $book_list = $daoBook->getBooks();

    $message = getMessage();

    $data = [
        'template' => 'tpl/main2.html',
        'contentPath' => 'book_list.html',
        'books' => $book_list,
        'message' => $message
    ];
    print renderTemplate('tpl/main2.html', $data);
}

elseif ($command === 'show_book_form') {
    $authors_list = $daoAuthor->get_authors();
    $data = [
        'command' => 'submit_book_form',
        'template' => 'tpl/main2.html',
        'isEditForm' => false,
        'contentPath' => 'book_form.html',
        'authors' => $authors_list,
    ];
    print renderTemplate('tpl/main2.html', $data);}

elseif($command == 'submit_book_form') {

    $book = createBook();

    $errors = validateBook($book);

    $authors_list = $daoAuthor->get_authors();

    if(empty($errors)) {
        $daoBook->saveBook($book);
        header('HTTP/1.1 302 See Result');
        header('Location: command=show_book_list&message=saved');
    }
    else {
        $data = [
            'command' => 'submit_book_form',
            'template' => 'tpl/main2.html',
            'isEditForm' => false,
            'contentPath' => 'book_form.html',
            'errors' => $errors,
            'book' => $book,
            'authors' => $authors_list,
        ];
        print renderTemplate('tpl/main2.html', $data);
    }
}

elseif ($command === 'book_edit') {

    $book = $daoBook->getBookById($_GET['id']);
    $authors_list = $daoAuthor->get_authors();

    $bookAuthors = $book->authors;
//    var_dump($bookAuthors[0]);
//    echo"<br>";
//    var_dump($bookAuthors[1]);
    $data = [
        'command' => 'submit_book_edit',
        'template' => 'tpl/main2.html',
        'isEditForm' => true,
        'contentPath' => 'book_form.html',
        'book' => $book,
        'authors' => $authors_list,
        'bookAuthors' => $bookAuthors,
        'id' => $_GET['id']
    ];

    print renderTemplate('tpl/main2.html', $data);
}

elseif($command == 'submit_book_edit') {


    $id = $_POST['id'];

    $book = createBook();
    $bookAuthors = $book->authors;
    $errors = validateBook($book);

    $authors_list = $daoAuthor->get_authors();

    if (isset($_POST['deleteButton'])) {
        $daoBook->bookRemove($id);

        header('HTTP/1.1 302 See Result');
        header('Location: command=show_book_list&message=removed');
    }
    elseif(empty($errors)) {
//        var_dump($bookAuthors);
        $daoBook->editBook($book, $id);
        header('HTTP/1.1 302 See result');
        header('Location: command=show_book_list&message=saved');
    }
    else {
//        var_dump($book);
        $data = [
            'command' => 'submit_book_edit',
            'template' => 'tpl/main2.html',
            'isEditForm' => false,
            'contentPath' => 'book_form.html',
            'errors' => $errors,
            'book' => $book,
            'authors' => $authors_list,
            'bookAuthors' => $bookAuthors
        ];
        print renderTemplate('tpl/main2.html', $data);
    }
}

elseif ($command === 'show_author_list') {

    $authors_list = $daoAuthor->get_authors();
    $message = getMessage();

    $data = [
//        'command' => 'show_author_list',
        'template' => 'tpl/main2.html',
        'contentPath' => 'author_list.html',
        'authors' => $authors_list,
        'message' => $message
    ];
    print renderTemplate('tpl/main2.html', $data);
}

elseif ($command === 'show_author_form') {
    $data = [
        'command' => 'submit_author_form',
        'template' => 'tpl/main2.html',
        'isEditForm' => false,
        'contentPath' => 'author_form.html'
    ];

    print renderTemplate('tpl/main2.html', $data);
}

elseif ($command === 'submit_author_form') {

    $author = createAnAuthor();

    $errors = validateAuthor($author);

//    var_dump($request);

    if (empty($errors)){
        $daoAuthor->saveAuthor($author);
        header('HTTP/1.1 302 See Result');
        header('Location: command=show_author_list&message=saved');}

    else{
        $data = [
            'command' => 'submit_author_form',
            'template' => 'tpl/main2.html',
            'isEditForm' => false,
            'contentPath' => 'author_form.html',
            'errors' => $errors,
            'author' => $author,

        ];
        print renderTemplate('tpl/main2.html', $data);
    }
}

elseif ($command === 'author_edit') {

    $author = $daoAuthor->getAuthorById($_GET['id']);
    $data = [
        'command' => 'submit_author_edit',
        'template' => 'tpl/main2.html',
        'isEditForm' => true,
        'contentPath' => 'author_form.html',
        'author' => $author,
        'id' => $author->id
    ];

    print renderTemplate('tpl/main2.html', $data);
}

elseif ($command === 'submit_author_edit') {

    $id = $_POST['id'];

    $author = createAnAuthor();
    $errors = validateAuthor($author);

    if (isset($_POST['deleteButton'])) {
        $daoAuthor->authorRemove($id);

        header('HTTP/1.1 302 See Result');
        header('Location: command=show_author_list&message=removed');
    }

    elseif (empty($errors)){
        $daoAuthor->authorEdit($author, $id);
        header('HTTP/1.1 302 See Result');
        header('Location: command=show_author_list&message=saved');
    }

    else{
        $data = [
            'command' => 'submit_author_form',
            'template' => 'tpl/main2.html',
            'isEditForm' => false,
            'contentPath' => 'author_form.html',
            'errors' => $errors,
            'author' => $author,

        ];
        print renderTemplate('tpl/main2.html', $data);
    }
}

else {

    echo "Programming error!";
//    echo'<br>';var_dump($request);
    header('HTTP/1.1 404 Something Fishy');
    throw new Error('programming error');
}
