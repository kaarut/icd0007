<?php

class MenuItem {
    public $id;
    public $name;
    public $subItems = [];

    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }

    public function addSubItem($subItem) {
        $this->subItems[] = $subItem;
    }
}

