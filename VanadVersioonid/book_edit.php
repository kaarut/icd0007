<?php
require_once("functions.php");

    $connection = pdo_mysql();
    $id = isset($_GET["id"]) ? $_GET["id"]: "";

    $data = getBookById($connection, $id);
    $authorOne = $data[0]['firstName'] .' '. $data[0]['lastName'];
    $authorTwo = $data[1]['firstName'] .' '. $data[1]['lastName'];

    foreach ($data as $book){}

    $authorios = [];

    $authors = getAuthorNames($connection);
    foreach ($authors as $author){array_push($authorios, $author);}

    $errors = [];
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        if (isset($_POST['deleteButton'])){
            deletePost($id, 'books', $connection);
            header("Location: index.php?success=Kustutatud");}
        else {
            if (isset($_POST['submitButton'])){
                $errors = saveValid('books');
                if (empty($errors)) {
                    editPost($id, 'books', $connection);
                    header("Location: index.php?success=Muudetud");
                }
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bookshelf</title>
    <link type="text/css" rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
<?php require 'parts/menu.html' ?>
<main>
    <div class="container">
        <?php foreach ($errors as $error): ?>
            <div id="error-block" class="error"><?= $error ?></div>
            <br>
        <?php endforeach; ?>
    </div>
    <div class="book_form">
        <form action="book_edit.php?id=<?=$id?>" method="post" name="book_add">
            <div class="cell">
                <label for="title">Pealkiri:</label>
                <input id="title" name="title" type="text"
                       value="<?= $book["title"];?>">
            </div>
            <div class="cell">
                <label for="author1">Autor I: </label>
                <select id="author1" name="author1">
                    <option></option>
                    <!--                    <option>--><?//= $authorTwo ?><!--</option>-->
                    <?php foreach ($authorios as $name): ?>
                        <option value="<?=$name['id']?>"><?= $name['firstName'] . ' '. $name['lastName']?> </option>
                <?php endforeach;?>
                </select>
            </div>
            <div class="cell">
                <label for="author2">Autor II: </label>
                <select id="author2" name="author2">
                    <option></option>
<!--                    <option>--><?//=$authorOne  ?><!--</option>-->
                    <?php foreach ($authorios as $name): ?>
                    <option value="<?= $name['id']?>"><?= $name['firstName'] . ' '. $name['lastName']?> </option>
               <?php endforeach;?>
                </select>
            </div>
            <div class="cell">
                <label for="grade">Hinnang: </label>
                <?php
                $arr = array(1,2,3,4,5); $grade = $book["grade"]; foreach($arr as $int)
                    if ($int != $grade)
                        echo "<input name='grade' value='$int' type='radio'>$int";
                    else
                        echo "<input name='grade' checked value='$int' type='radio'>$int";
                ?>
            </div>
            <div class="cell">
                <label for="read">Loetud: </label>
                <input id="read" value="1" name="isRead" type="checkbox"
                <?php if ($book["isRead"] == '1') {echo "checked";}?>>
            </div>
            <div class="button_cell">
                <input id="delete" name="deleteButton" type="submit" value="Kustuta">
            </div>
            <div class="button_cell">
                <input name="submitButton" type="submit" value="Kinnita">
            </div>
        </form>
    </div>
</main>
<?php require 'parts/footer.html' ?>
</body>
</html>