<?php
    require_once("functions.php");

    $connection = pdo_mysql();
    $id = isset($_GET["id"]) ? $_GET["id"]: "";

    $errors = [];
    $table = 'authors';

    $data = getPostById($connection, $table, $id);
    foreach ($data as $row) {}

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        if (isset($_POST['deleteButton'])){
            deletePost($id, $table, $connection);
            header("Location: author_view.php?success=Kustutatud");}
        else {
            if (isset($_POST['submitButton'])){
                $errors = saveValid($table);
                if (empty($errors)) {
                    editPost($id, $table, $connection);
                    header("Location: author_view.php?success=Muudetud");
                }
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bookshelf</title>
    <link type="text/css" rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link type="text/js" href="functions.js">
</head>
<body>
<?php require 'parts/menu.html' ?>
<main>
    <div class="container">
        <?php foreach ($errors as $error): ?>
            <div id="error-block" class="error"><?= $error ?></div>
            <br>
        <?php endforeach; ?>
    </div>
    <div class="book_form" >
        <form method="post" name="author_add">
            <div class="cell">
                <label for="firstName">Eesnimi:</label>
                <input id="firstName" name="firstName" type="text"
                       value="<?= $row['firstName'] ?>">
            </div>
            <div class="cell">
                <label for="lastName">Perekonnanimi</label>
                <input id="lastName" name="lastName" type="text"
                       value="<?=  $row['lastName'] ?>">
            </div>
            <div class="cell">
                <label for="grade">Hinnang: </label>
                <?php
                $arr = array(1,2,3,4,5); $grade =  $row['grade']; foreach($arr as $int)
                    if ($int != $grade)
                        echo "<input name='grade' value='$int' type='radio'>$int";
                    else
                        echo "<input name='grade' checked value='$int' type='radio'>$int";
                ?>
            </div>
            <div class="button_cell">
                <input id="delete" name="deleteButton" type="submit" value="Kustuta">
            </div>
            <div class="button_cell">
                <input name="submitButton" type="submit" value="Kinnita">
            </div>
        </form>
    </div>
</main>
<?php require 'parts/footer.html' ?>
</body>
</html>