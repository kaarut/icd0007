<?php

function printMenu($items, $level = 0) {
    $padding = str_repeat(' ', $level * 3);
    foreach ($items as $item) {
        printf("%s%s\n", $padding, $item->name);
        if ($item->subItems) {
            printMenu($item->subItems, $level + 1);
        }
    }
}

function getConnectionWithData($dataFile) {
    $conn = new PDO('sqlite::memory:');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statements = explode(';', join('', file($dataFile)));

    foreach ($statements as $statement) {
        $conn->prepare($statement)->execute();
    }

    return $conn;
}
