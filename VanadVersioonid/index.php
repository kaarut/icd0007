<?php
    require_once("functions.php");
    $connection = pdo_mysql();
    $books = getBooks($connection);
    $success = isSuccess();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bookshelf</title>
    <link type="text/css" rel="stylesheet" href="style.css">
    <script src="functions.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
<?php require 'parts/menu.html' ?>
<main>
    <?=$success?>
    <div id="book_table">
        <table>
            <tr>
                <th>Pealkiri</th>
                <th>Autor</th>
                <th>Hinne</th>
            </tr>
            <?php foreach ($books as $row): ?>
                <tr>
                    <td><a href="book_edit.php?id=<?=$row["id"]?>"><?= $row["title"] ?></a></td>
                    <td><?= $row['authors'] ?></td>
                    <td><?= str_repeat('★', (int)$row["grade"])?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</main>
<?php require 'parts/footer.html' ?>
</body>
</html>