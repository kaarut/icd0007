<?php

require_once '../ex4/functions.php';

$conn = getConnectionWithData('data.sql');

$stmt = $conn->prepare("SELECT student.id, student.name, grade.grade from grade left join student on grade.id=student.id");

$stmt->execute();

$result = [];

foreach ($stmt as $student) {
    $id = $student['id'];
    $name = $student['name'];
    $grade = $student['grade'];

    $result [$name] [] = $grade;

}

print_r($result);

foreach ($result as $name => $grades) {
    $std = standardDeviation($grades);
    print "$name: $std\n";
}
